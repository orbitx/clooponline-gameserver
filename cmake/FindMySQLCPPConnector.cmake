# set search hint directories
set(
     MySQLCPPConnector_POSSIBLE_ROOT_PATHS
     $ENV{MySQLCPPConnector_ROOT}
     /usr/local
     /usr
   )


# find MySQLCPPConnector include directory
# =================================================================================

find_path(
  MySQLCPPConnector_INCLUDE_DIR
  NAME          cppconn/connection.h
  HINTS         ${MySQLCPPConnector_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "include"
)

if(NOT MySQLCPPConnector_INCLUDE_DIR)
  message(STATUS "Checking for MySQL C++ connector... no")
  message(STATUS "Could not find include path for MySQL C++ connector, try setting MySQLCPPConnector_ROOT")
  return()
endif()


# find MySQLCPPConnector libraries
# =================================================================================

# library for static builds
find_library(
  MySQLCPPConnector_LIBRARY_STATIC
  NAMES          libmysqlcppconn-static.a
  HINTS          ${MySQLCPPConnector_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64"
  DOC            "MySQL C++ connector static library"
)

# library for dynamic builds
find_library(
  MySQLCPPConnector_LIBRARY_DYNAMIC
  NAMES          mysqlcppconn
  HINTS          ${MySQLCPPConnector_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64"
  DOC            "MySQL C++ connector shared object library"
)

# create library name for linking
set(MySQLCPPConnector_LIBRARIES "")
if(MySQLCPPConnector_LIBRARY_DYNAMIC)
  set(MySQLCPPConnector_LIBRARIES "${MySQLCPPConnector_LIBRARY_DYNAMIC}")
elseif(MySQLCPPConnector_LIBRARY_STATIC)
  set(MySQLCPPConnector_LIBRARIES "${MySQLCPPConnector_LIBRARY_STATIC}")
endif()
set(MySQLCPPConnector_LIBRARIES "${MySQLCPPConnector_LIBRARIES};dl")

# check the result
if(NOT MySQLCPPConnector_LIBRARIES)
  message(STATUS "Checking for MySQL C++ connector... no")
  message(STATUS "MySQL C++ connector's include directory: ${MySQLCPPConnector_INCLUDE_DIR}")
  message(FATAL_ERROR "Could not find MySQL C++ connector's library")
  return()
endif()

# extract the library directory
set(MySQLCPPConnector_LIBRARY_DIR_STATIC "")
set(MySQLCPPConnector_LIBRARY_DIR_DYNAMIC   "")
if(MySQLCPPConnector_LIBRARY_STATIC)
  get_filename_component(MySQLCPPConnector_LIBRARY_DIR_STATIC ${MySQLCPPConnector_LIBRARY_STATIC} PATH)
endif()
if(MySQLCPPConnector_LIBRARY_DYNAMIC)
  get_filename_component(MySQLCPPConnector_LIBRARY_DIR_DYNAMIC ${MySQLCPPConnector_LIBRARY_DYNAMIC} PATH)
endif()
set(MySQLCPPConnector_LIBRARY_DIR ${MySQLCPPConnector_LIBRARY_DIR_STATIC} ${MySQLCPPConnector_LIBRARY_DIR_DYNAMIC})


# MySQLCPPConnector runtime libraries
# =================================================================================

find_file(
           MySQLCPPConnector_RUNTIME_FILES cppconn.dll
           HINTS                   ${MySQLCPPConnector_POSSIBLE_ROOT_PATHS}
           PATH_SUFFIXES           "bin"
         )


# everything is found. just finish up
# =================================================================================

set(MySQLCPPConnector_FOUND TRUE CACHE BOOL "Whether MySQL C++ connector is found on the system or not")
set(MySQLCPPConnector_INCLUDE_DIR ${MySQLCPPConnector_INCLUDE_DIR} CACHE PATH "MySQL C++ connector's include directory")
set(MySQLCPPConnector_LIBRARIES ${MySQLCPPConnector_LIBRARIES} CACHE FILEPATH "MySQL C++ connector's libraries for linking against")
set(MySQLCPPConnector_LIBRARY_DIR ${MySQLCPPConnector_LIBRARY_DIR} CACHE PATH "MySQL C++ connector's library directory")
set(MySQLCPPConnector_RUNTIME_FILES ${MySQLCPPConnector_RUNTIME_FILES_DEBUG} CACHE FILEPATH "Runtime files of the MySQL C++ connector's library")

message(STATUS "Checking for MySQL C++ connector... yes")
