# set search hint directories
set(
     Gloox_POSSIBLE_ROOT_PATHS
     $ENV{Gloox_ROOT}
     /usr/local
     /usr
   )


# find Gloox include directory
# =================================================================================

find_path(
  Gloox_INCLUDE_DIR
  NAME          Gloox/Gloox.h
  NAME          gloox/gloox.h
  HINTS         ${Gloox_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES "include" "Gloox"
)

if(NOT Gloox_INCLUDE_DIR)
  message(STATUS "Checking for Gloox... no")
  message(STATUS "Could not find include path for Gloox, try setting Gloox_ROOT")
  return()
endif()


# find Gloox libraries
# =================================================================================

# library for debug builds
find_library(
  Gloox_LIBRARY_DEBUG
  NAMES          Gloox_d gloox_d
  HINTS          ${Gloox_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64"
  DOC            "Gloox library for debug builds"
)

# library for release builds
find_library(
  Gloox_LIBRARY_RELEASE
  NAMES          Gloox gloox
  HINTS          ${Gloox_POSSIBLE_ROOT_PATHS}
  PATH_SUFFIXES  "lib" "lib64" "lib/release"
  DOC            "Gloox library for release builds"
)

# create library name for linking
set(Gloox_LIBRARY "")
if(Gloox_LIBRARY_DEBUG AND Gloox_LIBRARY_RELEASE)
  set(Gloox_LIBRARY "optimized;${Gloox_LIBRARY_RELEASE};debug;${Gloox_LIBRARY_DEBUG}")
elseif(Gloox_LIBRARY_DEBUG)
  set(Gloox_LIBRARY "${Gloox_LIBRARY_DEBUG}")
elseif(Gloox_LIBRARY_RELEASE)
  set(Gloox_LIBRARY "${Gloox_LIBRARY_RELEASE}")
endif()

# check the result
if(NOT Gloox_LIBRARY)
  message(STATUS "Checking for Gloox... no")
  message(STATUS "Gloox include directory: ${Gloox_INCLUDE_DIR}")
  message(STATUS "Could not find Gloox library")
  return()
endif()

# extract the library directory
set(Gloox_LIBRARY_DIR_DEBUG   "")
set(Gloox_LIBRARY_DIR_RELEASE "")
if(Gloox_LIBRARY_DEBUG)
  get_filename_component(Gloox_LIBRARY_DIR_DEBUG ${Gloox_LIBRARY_DEBUG} PATH)
endif()
if(Gloox_LIBRARY_RELEASE)
  get_filename_component(Gloox_LIBRARY_DIR_RELEASE ${Gloox_LIBRARY_RELEASE} PATH)
endif()
set(Gloox_LIBRARY_DIR ${Gloox_LIBRARY_DIR_DEBUG} ${Gloox_LIBRARY_DIR_RELEASE})


# Gloox runtime libraries
# =================================================================================

find_file(
           Gloox_RUNTIME_FILES_DEBUG Gloox_d.dll
           HINTS                   ${Gloox_POSSIBLE_ROOT_PATHS}
           PATH_SUFFIXES           "bin" "bin/debug"
         )

find_file(
           Gloox_RUNTIME_FILES_RELEASE Gloox.dll
           HINTS                     ${Gloox_POSSIBLE_ROOT_PATHS}
           PATH_SUFFIXES             "bin" "bin/release"
         )


# everything is found. just finish up
# =================================================================================

set(Gloox_FOUND TRUE CACHE BOOL "Whether Gloox is found on the system or not")
set(Gloox_INCLUDE_DIR ${Gloox_INCLUDE_DIR} CACHE PATH "Gloox include directory")
set(Gloox_LIBRARY ${Gloox_LIBRARY} CACHE FILEPATH "Gloox library for linking against")
set(Gloox_LIBRARY_DIR ${Gloox_LIBRARY_DIR} CACHE PATH "Gloox library directory")
set(Gloox_RUNTIME_FILES_DEBUG ${Gloox_RUNTIME_FILES_DEBUG} CACHE FILEPATH "Runtime files of the Gloox library")
set(Gloox_RUNTIME_FILES_RELEASE ${Gloox_RUNTIME_FILES_RELEASE} CACHE FILEPATH "Runtime files of the Gloox library")

message(STATUS "Checking for Gloox... yes")
