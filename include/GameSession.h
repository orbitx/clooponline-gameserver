#pragma once

#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <mutex>
#include <set>
#include <packet_types.h>
#include <ThriftParser.h>
#include <Router.h>

#define THROW_ERROR_CODE(ERROR_ENUM_NAME) \
      throw ThriftException(ExceptionCode::ERROR_ENUM_NAME);

#define HANDLE_METHOD_HEAD(METHOD) \
      auto treqPacket = (ThriftParser*)reqPacket; \
      auto trepPacket = (ThriftParser*)repPacket; \
      if (! treqPacket->__isset.p##METHOD) \
      { \
         THROW_ERROR_CODE(INVALID_PACKET) \
      } \
      METHOD& innerpack = treqPacket->p##METHOD; \

class GameServer;
class PacketParser;
class GSProfile;
typedef std::shared_ptr<const GSProfile> GSProfilePtr;

enum gameState
{
   GAME_STATE_INFANCY = 0,
   GAME_STATE_WAITING_FOR_START,
   GAME_STATE_START_COUNTDOWN,
   GAME_STATE_PLAYING,
   GAME_STATE_LOCKED,
   GAME_STATE_ENDED
};

class GameSession
{
public:
   GameSession(GameServer* server, uint ID, GSProfilePtr creator, uint appID, const clooponline::packet::CreateSession& cs);
   GameSession(GameServer* server, GSProfilePtr creator, uint appID, const clooponline::packet::Subscribe& subs);
   virtual ~GameSession();

   uint  getAppID() { return mAppID; }
   uint  getID() { return mID; }
   std::vector<GSProfilePtr> getParticipants() { return mParticipants; }
   std::string getLabel() { return mLabel; }
   uint getPlayersNum() { return uint(mParticipants.size()); }
   bool filter(std::map<std::string, std::string> params);
   std::map<uint, uint> getCurrentPlayersNum() { return mSidetoPlayerNum; }
   std::map<uint, uint> getSideCapacities() { return mSidetoMaxPlayers; }
   bool isManager(const GSProfilePtr& client) { return client == mManager; }
   bool hasParticipated(uint64_t uid) { return std::find(mJoinedSoFar.begin(), mJoinedSoFar.end(), uid) != mJoinedSoFar.end(); }
   static uint getLiveSessionsCount() { return LiveSessionsCount; }

   void
   RPCCall(GSProfilePtr client, clooponline::packet::RPCCall& reqPacket, ThriftParser* repPacket);
   void setProperties(GSProfilePtr client, clooponline::packet::SetProperties& reqPacket, ThriftParser* repPacket);
   void getProperties(GSProfilePtr client, clooponline::packet::GetProperties& reqPacket, ThriftParser* repPacket);
   void terminate(GSProfilePtr client, clooponline::packet::Terminate& reqPacket, ThriftParser* repPacket);
   bool canJoin(GSProfilePtr client, const clooponline::packet::JoinSession& joinParams);
   void joinPlayer(GSProfilePtr client, const clooponline::packet::JoinSession& joinParams, ThriftParser* repPacket);
   bool canJoin(GSProfilePtr client, const clooponline::packet::Subscribe& subs);
   void joinPlayer(GSProfilePtr client, const clooponline::packet::Subscribe& subs);
   void changeSide(GSProfilePtr client, clooponline::packet::ChangeSide& reqPacket, ThriftParser* repPacket);
   void setReady(GSProfilePtr client, clooponline::packet::SetReady& reqPacket, ThriftParser* repPacket);
   void startSession(GSProfilePtr client, clooponline::packet::Start& reqPacket, ThriftParser* repPacket);
   void cancelStartSession(GSProfilePtr client, clooponline::packet::CancelStart& reqPacket, ThriftParser* repPacket);
   void tempLeave(GSProfilePtr client, clooponline::packet::TemporaryLeave& reqPacket, ThriftParser* repPacket);
   void handleActiveSession(GSProfilePtr client, clooponline::packet::HandleActiveSession& reqPacket, ThriftParser* repPacket);
   void changeConfigs(GSProfilePtr client, clooponline::packet::ChangeConfigs reqPacket, ThriftParser* repPacket);
   void managerKick(GSProfilePtr client, clooponline::packet::ManagerKick& reqPacket, ThriftParser* repPacket);
   void userDisconnected(GSProfilePtr client);
   void leaveSession(GSProfilePtr client);
   float getAverageIndicator();
   void promoteToActiveSession(uint ID);
   void fillSessionConfigs(clooponline::packet::SessionConfigs& sc);
   void fillJoinSessionObject(clooponline::packet::JoinSessionResponse& joinObject, uint rpcFromIndex = 0, uint propertiesFromIndex = 0);

   bool operator < (const GameSession& right) const { return mID < right.mID;}

protected:
   std::vector<GSProfilePtr> mParticipants;
   std::map<uint, uint>      mIDtoSide;
   std::map<uint, bool>      mIDtoReady;
   std::vector<GSProfilePtr> mTempLefts;
   std::map<uint, ulong>     mDCClientIDtoTimerInd;
   std::vector<uint64_t>     mJoinedSoFar;
   std::set<ulong>           mPendingTimerIndices;
   std::string               mLabel;
   ulong                     mStartTimerIndex;
   clooponline::packet::DOMNode mProperties;
   uint                         mPropertiesRevisionIndex;
   std::vector<ThriftParser>    mChachedRPCs;
   uint                         mLastRPCIndex;
   std::string                  mPassword;

   uint           mAppID;
   GameServer*    mGameServer;
   std::map<std::string, std::string> mOptions;
   
   std::map<uint, uint>  mSidetoMaxPlayers;
   std::map<uint, uint>  mSidetoPlayerNum;
   uint                  mMaxAllowedPlayers;
   gameState      mState;
   uint           mID;
   GSProfilePtr   mManager;
   bool           mAlwaysAcceptSubscription;
   bool           mAllShouldBeReady;
   bool           mPlayersCanCancel;
   bool           mPlayersCanChangeSideAfterStart;
   clooponline::packet::DisconnectionActionType::type mDCBehavior;
   uint           mDCTimeout;
   uint           mMinPlayersToMatch;
   float          mAverageIndicator;
   float          mMaxAllowedIndicatorDistance;
   static uint    LiveSessionsCount;

   void initialize(GSProfilePtr client, const clooponline::packet::SessionConfigs& configs, uint creatorSide);

   void leftSession(GSProfilePtr client, clooponline::packet::LeavingReason::type reason);
   void tempLeftSession(GSProfilePtr client);
   void fillDCSides();
   void defineSide(uint sideid, uint maxPlayers);
   void assignManager(GSProfilePtr client = 0);
   void reconnectTimeout(GSProfilePtr client);
   void doStartCheckings();
   void addParticipant(GSProfilePtr client, uint side);
   void DCHandled(GSProfilePtr client);
   void startGame();
   void timeoutDone(ulong timerind);
   void endSession();

   GSProfilePtr getProfileFromUID(uint64_t UID);
   GSProfilePtr getProfileFromCID(uint CID);

   void broadcast(PacketParser* msg, uint exceptionID = 0);
};