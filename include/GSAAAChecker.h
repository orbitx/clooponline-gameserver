#pragma once

#include "AAAChecker.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <cxxtools/json/httpclient.h>
#include <cxxtools/remoteprocedure.h>
#include <atomic>
#include <mutex>
#include <string>
#include <PacketParser.h>
#include <unordered_map>

class Router;

struct GSProfile : public Profile
{
   GSProfile(uint cid)
      : Profile(cid)
   { }

   uint64_t UID;
   uint     AppID;
};

struct GSProfileFactory : public ProfileFactory
{
   virtual std::shared_ptr<Profile> createOne(uint cid)
   {
      return std::make_shared<GSProfile>(cid);
   }
};

class GSAAAChecker : public AAAChecker
{
private:
   struct RPCResponse
   {
      std::string result;
      std::string reason;
   };

   struct GetOTPInfoResponse : public RPCResponse
   {
      uint64_t UID;
      uint     AppID;
   };

   struct GetAuthorizedAppsResponse : public RPCResponse
   {
      std::vector<uint> AuthAppIDs;
   };

   friend void operator>>= (const cxxtools::SerializationInfo& si, GSAAAChecker::RPCResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, GSAAAChecker::GetOTPInfoResponse& re);
   friend void operator>>= (const cxxtools::SerializationInfo& si, GSAAAChecker::GetAuthorizedAppsResponse& re);

public:

   GSAAAChecker(Router* router, std::string aaaip, uint16_t aaaport, std::string urlpath, std::string gstoken);
   virtual void onPacketFromUnidentified(std::shared_ptr<const Profile> client, PacketParser* reqPacket, PacketParser* repPacket);

private:
   cxxtools::json::HttpClient mRPCClient;
   cxxtools::RemoteProcedure<GetOTPInfoResponse, std::string> mGetOTPInfoCaller;

   std::string mGSToken;
   std::vector<uint> mAuthorizedApps;
   std::atomic<uint> mIdentifiedCount;
   std::chrono::steady_clock::time_point  mLastAuthorizedAppsGet;
   std::unordered_map<uint, ulong> mIDtoEventID;
   bool mAuthorized;

   void identifyTimeout(std::shared_ptr<const Profile> client);
   void checkAuthorizedApps();

   void connectHandler(std::shared_ptr<const Profile> client) override;
   void disconnectHandler(std::shared_ptr<const Profile> client) override;
};