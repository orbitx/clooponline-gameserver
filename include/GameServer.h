#pragma once

#include <Router.h>
#include <GameSession.h>
#include <map>
#include <iostream>
#include <utility>
#include <ThriftBuffer.h>

#define FIND_GAME_SESSION(PACKET_NAME) \
      auto gs = getGameSession((uint)innerpack.sessionID); \
      if (!gs) \
      { \
         if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
         { \
            *loggerptr << "Client " << client->CID \
                       << " sent a \"" << (PACKET_NAME) << "\" packet but was not assigned to that specific session."; \
            loggerptr->flushLogBuffer(); \
         } \
         THROW_ERROR_CODE(INVALID_SID) \
      }

class GSAAAChecker;
class GSProfile;
typedef std::shared_ptr<const GSProfile> GSProfilePtr;

class GameServer : public Router
{
   struct SubsInfo
   {
      uint CID;
      uint64_t TID; //trackID
      ulong TimeEventID;
      clooponline::packet::Subscribe Configs;
      std::vector<std::shared_ptr<GameSession>> PossibleSessions;
      bool IsOnPriority{false};

      struct PushToPossibleSessions
      {
         explicit PushToPossibleSessions(std::shared_ptr<GameSession> gs)
         {
            toPush = std::move(gs);
         }

         void operator()(SubsInfo& inf)
         {
            inf.PossibleSessions.push_back(toPush);
         }

      private:
         std::shared_ptr<GameSession> toPush;
      };

      struct ChangePriority
      {
         explicit ChangePriority(bool p)
         {
            priority = p;
         }

         void operator()(SubsInfo& inf)
         {
            inf.IsOnPriority = priority;
         }

      private:
         bool priority;
      };

      struct ChangeTimeEventID
      {
         explicit ChangeTimeEventID(ulong teid)
         {
            newTEID = teid;
         }

         void operator()(SubsInfo& inf)
         {
            inf.TimeEventID = newTEID;
         }

      private:
         ulong newTEID;
      };

      struct ByCID {};
      struct ByTID {};
   };

   typedef boost::multi_index_container<SubsInfo,
      boost::multi_index::indexed_by<
         boost::multi_index::ordered_non_unique<
            boost::multi_index::tag<SubsInfo::ByCID>,
            boost::multi_index::member<SubsInfo, uint, &SubsInfo::CID>
         >,
         boost::multi_index::ordered_unique<
            boost::multi_index::tag<SubsInfo::ByTID>,
            boost::multi_index::member<SubsInfo, uint64_t, &SubsInfo::TID>
         >
      >
   > SubscribleContainer;

   typedef SubscribleContainer::index<SubsInfo::ByCID>::type SubscribeByCID;
   typedef SubscribleContainer::index<SubsInfo::ByTID>::type SubscribeByTID;

public:
   GameServer(uint16_t wsPort, uint16_t socketPort, std::string loggeraddr, std::string aaaip, uint16_t aaaport,
              std::string aaaurlpath, std::string gstoken, ushort threadsNum = 8);

   ~GameServer() override;

   void endSession(uint sid);
   void clientLeftSession(GSProfilePtr client, uint sid);
   void defineTempLeftSession(GSProfilePtr client, uint sid);
   void removeTempLeftSession(GSProfilePtr client, uint sid);
   void joinedSession(GSProfilePtr client, uint sid);
   void cleanShutdown();
   virtual void monitoringIterated();
   void run()
   {
      Router::run<ThriftBuffer, ThriftParserFactory>(true);
   }

private:

   struct Hash_CID
   {
      size_t operator()(const uint &i) const
      {
         return std::hash<size_t>()(i % 10000);
      }
   };

   std::unordered_map<uint, std::shared_ptr<GameSession>, Hash_CID> mManualSessions;
   std::unordered_map<uint, std::shared_ptr<GameSession>, Hash_CID> mMatchedSessions;
   std::multimap<uint, std::shared_ptr<GameSession>> mIDtoSessions;
   std::multimap<ulong, std::shared_ptr<GameSession>> mTempLeftMap;

   uint mLastGameSessionID;
   uint generateNextGameSessionID();

   std::atomic<uint64_t>   mLastSubsTrackID;
   SubscribleContainer     mSubscribePool;
   SubscribeByCID&         mSubscribeByCID;
   SubscribeByTID&         mSubscribeByTID;

   GSAAAChecker* mGSAAAChecker;

   //monitoring variables
   uint  mSessionCreatedCount;
   uint  mSessionStartedCount;
   uint  mSessionEndedCount;
   uint  mHandledDCCount;
   uint  mJoinedSessionCount;
   uint  mQueuedJobs;
   uint  mLiveSessions;

   std::shared_ptr<GameSession>  getGameSession(uint sid);
   std::vector<std::shared_ptr<GameSession>> getGameSessions(GSProfilePtr client);
   std::vector<std::shared_ptr<GameSession>> getTempLeftSessions(GSProfilePtr client);

   void createSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void joinSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void subscribe(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void unsubscribe(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void changeSide(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void changeConfigs(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void leaveSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void tempLeave(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void sessionList(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);

   void clientRPCCall(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void setReady(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void startSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void cancelStartSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void setSessionProperties(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void getSessionProperties(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void managerKick(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void getActiveSessions(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void handleActiveSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);
   void terminateSession(ProfilePtr CID, PacketParser* reqPacket, PacketParser* repPacket);

   void onUserDC(ProfilePtr CID);
   void onUserConnect(ProfilePtr CID);
   void promoteSession(std::shared_ptr<GameSession> gsptr);
   void fillActiveSessions(GSProfilePtr client, ThriftParser* packet);

   void subscriptionPoolTimedOut(uint64_t TID);
};