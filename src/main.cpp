#include <iostream>
#include <fstream>
#include <GameServer.h>
#include <MonitoringManager.h>
#include <JsonParser.h>
#include <Logger.h>
#include <Singleton.h>
#include <signal.h>
using namespace std;

GameServer* gsptr = nullptr;
shared_ptr<Logger> loggerptr = nullptr;
shared_ptr<MonitoringManager> monitorptr = nullptr;

void mySigIntHandler(int s)
{
   if (s == SIGINT || s == SIGQUIT)
   {
      if (s == SIGQUIT)
         gsptr->cleanShutdown();

      if (loggerptr)
         loggerptr->stop();

      if (monitorptr)
         monitorptr->stop();

      if (gsptr)
      {
         delete gsptr;
         gsptr = 0;
      }
   }
}

int main(int argc, char **argv) {
   signal(SIGINT, &mySigIntHandler);
   signal(SIGQUIT, &mySigIntHandler);

   string confFileAddr = "../config/gameserver.conf";
   
   for (int i = 1; i < argc; ++i)
   {
      stringstream ss(argv[i]);
      string opt; ss >> opt;
      
      if (opt == "-conf")
      {
         istringstream iss(argv[++i]);
         iss >> confFileAddr;
      }
      else
      {
         cout << "Unknown command \"" << opt << "\". Command skipped.\n";
      }
   }

   string loggeraddr, redisaddr, aaaaddr, redisip, aaaip, aaaurlpath, gstoken;
   uint16_t wsPort, socketPort, redisport, aaaport;
   uint8_t threadNum;
   string sevirity;

   try
   {
      ifstream confFile (confFileAddr);
      if (!confFile)
         throw runtime_error("config file \"" + confFileAddr + "\" not found.");
      
      string conf((istreambuf_iterator<char>(confFile)), istreambuf_iterator<char>());
      
      JsonParser mp;
      mp.deserialize(conf);

      loggeraddr = mp.getStringMember("logger_addr");
      redisaddr = mp.getStringMember("redis_addr");
      aaaaddr = mp.getStringMember("aaa_addr");
      aaaurlpath = mp.getStringMember("aaa_url_path");
      gstoken = mp.getStringMember("aaa_gs_token");

      wsPort = (uint16_t)mp.getUintMember("websocket_port");
      socketPort = (uint16_t)mp.getUintMember("socket_port");
      sevirity = mp.getStringMember("log_severity");
      threadNum = (uint8_t)mp.getUintMember("threads_number");

      istringstream raddr(redisaddr);
      getline(raddr, redisip, ':');
      raddr >> redisport;

      istringstream aaddr(aaaaddr);
      getline(aaddr, aaaip, ':');
      aaddr >> aaaport;
   }
   catch (exception& e)
   {
      cout << "The following error accured when reading config file: " << e.what() << endl;
      return 0;
   }

   LogSeverity ls = LOG_SEVERITY_DEBUG;
   if (sevirity == "trace")
      ls = LOG_SEVERITY_TRACE;
   else if (sevirity == "debug")
      ls = LOG_SEVERITY_DEBUG;
   else if (sevirity == "info")
      ls = LOG_SEVERITY_INFO;
   else if (sevirity == "warning")
      ls = LOG_SEVERITY_WARNING;
   else if (sevirity == "error")
      ls = LOG_SEVERITY_ERROR;
   else if (sevirity == "fatal")
      ls = LOG_SEVERITY_FATAL;
   else if (sevirity == "nolog")
      ls = LOG_SEVERITY_NO_LOG;

   loggerptr = Singleton<Logger>::construct("serverlogs", ls);
   monitorptr = Singleton<MonitoringManager>::construct(1, "ServerMonitoring", redisip, redisport);
   loggerptr->run();
   monitorptr->run();
   gsptr = new GameServer(wsPort, socketPort, loggeraddr, aaaip, aaaport, aaaurlpath, gstoken, threadNum);
   gsptr->run();

   while(gsptr)
   {
      this_thread::sleep_for(chrono::seconds(1));
   }
   
   return 0;
}
