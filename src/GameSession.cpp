#include <GameSession.h>
#include <GameServer.h>
#include <MonitoringManager.h>
#include <Singleton.h>
#include <Logger.h>
#include <ThriftException.h>
#include <GSAAAChecker.h>

#include <utility>

#define CHECK_IF_MANAGER(MANAGER, PACKET_NAME) \
      if ((MANAGER) != mManager) \
      { \
         if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
         { \
            *loggerptr << "Client " << (MANAGER)->CID \
                       << " attempted to do \"" << (PACKET_NAME) << "\" in session \"" << mID \
                       << "\" but was not assigned to be the manager."; \
            loggerptr->flushLogBuffer(); \
         } \
         THROW_ERROR_CODE(NOT_MANAGER) \
      }


#define CHECK_PARTICIPATION(CLIENT, PACKET_NAME) \
      if ((CLIENT) && find(mParticipants.begin(), mParticipants.end(), CLIENT) == mParticipants.end()) \
      { \
         if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
         { \
            *loggerptr << "Client " << (CLIENT)->CID \
                       << " is not a participant of session \"" << mID << "\" but packet \"" \
                       << (PACKET_NAME) << "\" was given for him."; \
            loggerptr->flushLogBuffer(); \
         } \
         THROW_ERROR_CODE(NOT_A_PARTICIPANT) \
      }

#define CHECK_STATE(CLIENT, EXPECTED_STATE, PACKET_NAME) \
      if (mState != (EXPECTED_STATE)) \
      { \
         if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
         { \
            *loggerptr << "Client " << (CLIENT)->CID \
                       << " sent a \"" << (PACKET_NAME) << "\" packet but session was in an invalid state."; \
            loggerptr->flushLogBuffer(); \
         } \
         THROW_ERROR_CODE(INVALID_SESSION_STATE) \
      }

#define CHECK_NOT_STATE(CLIENT, NOT_EXPECTED_STATE, PACKET_NAME) \
      if (mState == (NOT_EXPECTED_STATE)) \
      { \
         if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
         { \
            *loggerptr << "Client " << (CLIENT)->CID \
                       << " sent a \"" << (PACKET_NAME) << "\" packet but session was in an invalid state."; \
            loggerptr->flushLogBuffer(); \
         } \
         THROW_ERROR_CODE(INVALID_SESSION_STATE) \
      }

#define CHECK_EVER_PARTICIPATED(UID, PACKET_NAME) \
   if (find(mJoinedSoFar.begin(), mJoinedSoFar.end(), UID) == mJoinedSoFar.end()) \
   { \
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
      { \
         *loggerptr << "client \"" << (UID) \
                    << " was never a participant of session " << mID << " but \"" << (PACKET_NAME) << "\" was called for him."; \
         loggerptr->flushLogBuffer(); \
      } \
      THROW_ERROR_CODE(NOT_A_PARTICIPANT) \
   }

using namespace std;
using namespace clooponline::packet;

extern shared_ptr<Logger> loggerptr;
extern shared_ptr<MonitoringManager> monitorptr;

uint GameSession::LiveSessionsCount = 0;

GameSession::GameSession(GameServer* server, uint ID, GSProfilePtr creator, uint appID, const clooponline::packet::CreateSession& cs)
         : mGameServer(server), mID(ID), mAppID(appID)
{
   mState = GAME_STATE_WAITING_FOR_START;
   initialize(creator, cs.configs, uint(cs.side));
   ++LiveSessionsCount;

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Clinet " << creator->CID << " created session \"" << ID << "\".";
      loggerptr->flushLogBuffer();
   }
}

GameSession::GameSession(GameServer* server, GSProfilePtr creator, uint appID, const clooponline::packet::Subscribe& subs)
   : mGameServer(server), mAppID(appID)
{
   mState = GAME_STATE_INFANCY;
   initialize(move(creator), subs.configs, uint(subs.side));
   mAverageIndicator = float(subs.indicator);
   if (subs.__isset.maxIndicatorDistance)
      mMaxAllowedIndicatorDistance = float(subs.maxIndicatorDistance);
}

void GameSession::initialize(GSProfilePtr creator, const clooponline::packet::SessionConfigs& configs, uint creatorSide)
{
   mManager = nullptr;
   mLastRPCIndex = 0;
   mAppID = creator->AppID;
   mOptions = configs.options;
   mLabel = configs.label;
   mPassword = configs.password;
   mAlwaysAcceptSubscription = configs.alwaysAcceptJoin;
   mPlayersCanChangeSideAfterStart = configs.canChangeSideAfterStart;
   mDCBehavior = configs.disconnectionBehavior;
   mDCTimeout = (configs.__isset.disconnectionTimeout) ? uint(configs.disconnectionTimeout) : 60 ;
   mMinPlayersToMatch = uint(configs.minPlayerToMatch);
   mMaxAllowedPlayers = 0;
   mAverageIndicator = 0;
   mMaxAllowedIndicatorDistance = std::numeric_limits<float>::max();

   mProperties.type = ValueType::MAP;
   mProperties.__isset.children = true;
   mPropertiesRevisionIndex = 0;

   if (!configs.sideToCapacity.empty())
   {
      for (auto& s : configs.sideToCapacity)
      {
         if (s.first != 0)
            defineSide(uint(s.first), uint(s.second));
      }
   }
   else
   {
      defineSide(1, 0);
   }

   addParticipant(creator, creatorSide);
}

GameSession::~GameSession()
{
   endSession();
}

void GameSession::endSession()
{
   if (mState == GAME_STATE_ENDED)
      return;

   if (mState != GAME_STATE_INFANCY)
      --LiveSessionsCount;

   if (mState != GAME_STATE_INFANCY)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
      {
         *loggerptr << "Session \"" << mID << "\" ended.\n" << string(5, '\t')
                    << "There are " << GameSession::getLiveSessionsCount() << " live sessions ";
         loggerptr->flushLogBuffer();
      }
   }

   mState = GAME_STATE_ENDED;

   for (auto& p : mParticipants)
   {
      mGameServer->clientLeftSession(p, mID);
   }

   for (auto& t : mTempLefts)
   {
      mGameServer->removeTempLeftSession(t, mID);
      mGameServer->clientLeftSession(t, mID);
   }

   for (auto& d : mDCClientIDtoTimerInd)
   {
      mGameServer->timeoutDone(d.second);
   }

   for (auto& ind : mPendingTimerIndices)
   {
      mGameServer->timeoutDone(ind);
   }

   mGameServer->endSession(mID);
}

void GameSession::defineSide(uint sideid, uint maxPlayers)
{
   bool redefine = false;
   mMaxAllowedPlayers = 0;
   for (auto& s : mSidetoMaxPlayers)
   {
      if (s.first == sideid)
      {
         s.second = maxPlayers;
         redefine = true;
      }

      if (s.second == 0)
         mMaxAllowedPlayers = numeric_limits<uint>::max();
      else if (mMaxAllowedPlayers != numeric_limits<uint>::max())
         mMaxAllowedPlayers += s.second;
   }

   if (!redefine)
   {
      mSidetoPlayerNum[sideid] = 0;
      mSidetoMaxPlayers[sideid] = maxPlayers;
      if (mMaxAllowedPlayers != numeric_limits<uint>::max())
         mMaxAllowedPlayers += maxPlayers;
   }
}

void GameSession::userDisconnected(GSProfilePtr client)
{
   if (mState == GAME_STATE_PLAYING || mState == GAME_STATE_LOCKED)
   {
      auto pp = (ThriftParser*)mGameServer->createPacket();
      switch (mDCBehavior)
      {
         case DisconnectionActionType::KICK:
            leftSession(client, LeavingReason::DISCONNECTION);
            break;

         case DisconnectionActionType::TERMINATE:
            pp->setMessageType(PacketType::TERMINATED);
            pp->pTerminated.__set_sessionID(mID);
            pp->pTerminated.__set_reason(TerminationReason::DISCONNECTION);
            broadcast(pp, client->CID);
            endSession();
            break;

         case DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT:
         case DisconnectionActionType::CONTINUE_AND_TERMINATE_ON_TIMEOUT:
         case DisconnectionActionType::LOCK_AND_KICK_ON_TIMEOUT:
         case DisconnectionActionType::LOCK_AND_TERMINATE_ON_TIMEOUT:
         default:
            if (mDCTimeout > 0)
            {
               mDCClientIDtoTimerInd[client->CID] =
                  mGameServer->mThreadPool.AddEvent(bind(&GameSession::reconnectTimeout, this, client), mDCTimeout);
            }
            tempLeftSession(client);

            if ((mDCBehavior == DisconnectionActionType::LOCK_AND_KICK_ON_TIMEOUT ||
                mDCBehavior == DisconnectionActionType::LOCK_AND_TERMINATE_ON_TIMEOUT) &&
                mState == GAME_STATE_PLAYING)
            {
               pp->setMessageType(PacketType::LOCKED);
               pp->pLocked.__set_sessionID(mID);
               pp->pLocked.__set_UID(client->UID);
               broadcast(pp, client->CID);
               mState = GAME_STATE_LOCKED;
            }
            break;
      }
   }
   else
   {
      leftSession(client, LeavingReason::DISCONNECTION);
   }
}

void GameSession::reconnectTimeout(GSProfilePtr client)
{
   //find the previous clientID
   auto preit = std::find(mTempLefts.begin(), mTempLefts.end(), client);

   auto pp = (ThriftParser*)mGameServer->createPacket();
   //waiting is done, behave according to settings
   switch(mDCBehavior)
   {
      case DisconnectionActionType::LOCK_AND_TERMINATE_ON_TIMEOUT:
      case DisconnectionActionType::CONTINUE_AND_TERMINATE_ON_TIMEOUT:
         pp->setMessageType(PacketType::TERMINATED);
         pp->pTerminated.__set_sessionID(mID);
         pp->pTerminated.__set_reason(TerminationReason::DISCONNECTION);
         broadcast(pp, (*preit)->CID);
         endSession();
         break;

      case DisconnectionActionType::LOCK_AND_KICK_ON_TIMEOUT:
      case DisconnectionActionType::CONTINUE_AND_KICK_ON_TIMEOUT:
         leftSession(*preit, LeavingReason::DISCONNECTION);
         DCHandled(*preit);
         break;

      default:
         if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
         {
            *loggerptr << "reconnect timeout function called in session " << mID
                       << " but disconnection behavior is not one of the waiting ones.";
            loggerptr->flushLogBuffer();
         }
         break;
   }
}

void GameSession::setProperties(GSProfilePtr client, clooponline::packet::SetProperties& reqPacket, ThriftParser* repPacket)
{
   CHECK_PARTICIPATION(client, "set properties")
   CHECK_NOT_STATE(client, GAME_STATE_INFANCY, "set properties")
   CHECK_NOT_STATE(client, GAME_STATE_LOCKED, "set properties")

   repPacket->setMessageType(PacketType::SET_PROPERTIES_RESPONSE);
   boost::char_separator<char> sep(".");
   auto pchange = (ThriftParser*)mGameServer->createPacket();
   pchange->setMessageType(PacketType::PROPERTIES_CHANGED);
   for (auto& ins : reqPacket.properties)
   {
      clooponline::packet::DOMNode* nodeptr = &mProperties;
      clooponline::packet::DOMNode* parent = nullptr;
      boost::tokenizer<boost::char_separator<char>> tokens(ins.path, sep);

      SetPropertyResult::type resultFlag = SetPropertyResult::OK;
      bool isResultReserved = false;
      SetPropertyResult::type reservedResultFlag;
      int index;
      string childStr;
      bool justCreated = false;
      for (auto it = tokens.begin(); it != tokens.end(); ++it)
      {
         index = -1;
         childStr = "";
         size_t sb = it->find('[');
         if (sb != std::string::npos)
         {
            size_t eb = it->find(']', sb);
            if (eb == std::string::npos)
            {
               resultFlag = SetPropertyResult::ERROR_PATH_SYNTAX_ERROR;
               break;
            }

            string indstr = it->substr(sb+1, eb-sb-1);
            childStr = it->substr(0, sb);
            if (!indstr.empty() && std::find_if(indstr.begin(), indstr.end(), [](char c) { return std::isdigit(c) == 0; }) == indstr.end())
               index = atoi(indstr.c_str());
            else
            {
               resultFlag = SetPropertyResult::ERROR_INVALID_ARRAY_INDEX;
               break;
            }
         }
         else
         {
            childStr = *it;
         }

         auto child = nodeptr->children.find(childStr);
         if (child == nodeptr->children.end())
         {
            if (nodeptr->type != ValueType::MAP)
            {
               nodeptr->type = ValueType::MAP;
               nodeptr->__isset.array = false;
               nodeptr->array.clear();
               nodeptr->__isset.children = true;
               nodeptr->children.clear();
               resultFlag = SetPropertyResult::WARNING_TYPE_CHANGED;
            }
            child = nodeptr->children.insert(make_pair(childStr, DOMNode())).first;
            justCreated = true;

            if (!isResultReserved)
            {
               isResultReserved = true;
               reservedResultFlag = resultFlag;
            }
         }

         if (index == -1)
         {
            parent = nodeptr;
            nodeptr = &(child->second);
         }
         else
         {
            if (child->second.type != ValueType::ARRAY)
            {
               child->second.type = ValueType::ARRAY;
               child->second.__isset.array = true;
               child->second.array.clear();
               child->second.__isset.children = false;
               child->second.children.clear();
               resultFlag = SetPropertyResult::WARNING_TYPE_CHANGED;
            }

            if (index == child->second.array.size())
            {
               child->second.array.emplace_back();
               justCreated = true;
            }
            else if (index > child->second.array.size())
            {
               resultFlag = SetPropertyResult::ERROR_INDEX_OUT_OF_RANGE;
               break;
            }

            parent = &(child->second);
            nodeptr = &(child->second.array[index]);
         }
      }

      if (resultFlag < SetPropertyResult::ERROR_PATH_SYNTAX_ERROR)
      {
         switch (ins.operation)
         {
            case OperationType::MODIFY:
               if (!justCreated && nodeptr->type != ins.value.type)
                  resultFlag = SetPropertyResult::WARNING_TYPE_CHANGED;
               *nodeptr = ins.value;
               ins.cachedValue = DOMNode();
               ins.__isset.cachedValue = false;
               break;
            case OperationType::TRY_MODIFY:
               if (justCreated || *nodeptr == ins.cachedValue)
               {
                  *nodeptr = ins.value;
                  ins.operation = OperationType::MODIFY;
                  ins.cachedValue = DOMNode();
                  ins.__isset.cachedValue = false;
               }
               else
                  resultFlag = SetPropertyResult::ERROR_INVALID_CACHED_VALUE;
               break;
            case OperationType::DELETE:
               ins.cachedValue = DOMNode();
               ins.__isset.cachedValue = false;
               ins.value = DOMNode();
               ins.__isset.value = false;
               if (parent == nullptr)
               {
                  throw runtime_error("something went wrong in setting room properties");
               }
               if (parent->type == ValueType::ARRAY)
                  parent->array.erase(parent->array.begin() + index);
               else if (parent->type == ValueType::MAP)
                  parent->children.erase(childStr);
               else
               {
                  throw runtime_error("something went wrong in setting room properties");
               }
               break;
         }
      }

      if (resultFlag < SetPropertyResult::ERROR_PATH_SYNTAX_ERROR)
      {
         repPacket->pSetPropertiesResponse.resultFlags.push_back((isResultReserved) ? reservedResultFlag : resultFlag);
         pchange->pPropertiesChanged.changes.push_back(ins);
      }
      else
      {
         repPacket->pSetPropertiesResponse.resultFlags.push_back(resultFlag);
      }
   }

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Client \"" << client->CID << "\" set properties in session \"" << mID << "\".";
      loggerptr->flushLogBuffer();
   }

   if (!pchange->pPropertiesChanged.changes.empty())
   {
      pchange->pPropertiesChanged.UID = client->UID;
      pchange->pPropertiesChanged.revisionIndex = ++mPropertiesRevisionIndex;
      pchange->pPropertiesChanged.sessionID = mID;
      pchange->pPropertiesChanged.__set_serverTime(long(mGameServer->mThreadPool.getTime()));
      repPacket->pSetPropertiesResponse.__set_serverTime(pchange->pPropertiesChanged.serverTime);
      repPacket->pSetPropertiesResponse.__set_revisionIndex(pchange->pPropertiesChanged.revisionIndex);
      broadcast(pchange, client->CID);
   }
   mGameServer->sendPacket(client, repPacket);
}

void GameSession::getProperties(GSProfilePtr client, clooponline::packet::GetProperties& reqPacket, ThriftParser* repPacket)
{
   CHECK_NOT_STATE(client, GAME_STATE_INFANCY, "get properties")
   repPacket->setMessageType(PacketType::GET_PROPERTIES_RESPONSE);
   repPacket->pGetPropertiesResponse.properties = mProperties;
   mGameServer->sendPacket(client, repPacket);
}

void GameSession::addParticipant(GSProfilePtr client, uint side)
{
   mJoinedSoFar.push_back(client->UID);
   mParticipants.push_back(client);
   mIDtoSide[client->CID] = side;
   mIDtoReady[client->CID] = false;
   ++mSidetoPlayerNum[side];

   if (mParticipants.size() == 1 && mState != GAME_STATE_INFANCY)
      assignManager(client);

   if (side == 0 && mState == GAME_STATE_PLAYING)
      fillDCSides();
}

void GameSession::assignManager(GSProfilePtr client)
{
   if (mParticipants.empty())
   {
      mManager = nullptr;
      return;
   }

   mManager = (client == 0) ? mParticipants[rand() % mParticipants.size()] : client ;

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::BE_MANAGER);
   tp->pBeManager.__set_sessionID(mID);
   mGameServer->sendPacket(mManager, tp);
   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Client \"" << mManager->CID << "\" assigned to be manager in session \""
                 << mID << "\"";
      loggerptr->flushLogBuffer();
   }
}

void
GameSession::startSession(GSProfilePtr client, clooponline::packet::Start& reqPacket, ThriftParser* repPacket)
{
   CHECK_IF_MANAGER(client, "start session")
   CHECK_STATE(client, GAME_STATE_WAITING_FOR_START, "start session")

   mAllShouldBeReady = reqPacket.allShouldBeReadyFlag;
   mPlayersCanCancel = reqPacket.playersCanCancelFlag;

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Session \"" << mID << "\" is getting ready to start.";
      loggerptr->flushLogBuffer();
   }

   if (reqPacket.timeout == 0)
      doStartCheckings();
   else
   {
      for (auto& idr : mIDtoReady)
      {
         auto tp = (ThriftParser*)mGameServer->createPacket();
         tp->setMessageType(PacketType::READY);
         tp->pReady.__set_sessionID(mID);
         tp->pReady.__set_timeout(reqPacket.timeout);
         idr.second = false;
         mGameServer->sendPacket(idr.first, tp);
      }

      mStartTimerIndex = mGameServer->mThreadPool.AddEvent(bind(&GameSession::doStartCheckings, this), uint(reqPacket.timeout));
      mState = GAME_STATE_START_COUNTDOWN;
   }
}

void GameSession::cancelStartSession(GSProfilePtr client, clooponline::packet::CancelStart& reqPacket, ThriftParser* repPacket)
{
   CHECK_STATE(client, GAME_STATE_START_COUNTDOWN, "cancelStart")
   CHECK_PARTICIPATION(client, "cancelStart")

   if (client == mManager || mPlayersCanCancel)
   {
      mGameServer->timeoutDone(mStartTimerIndex);
      auto tp = (ThriftParser*)mGameServer->createPacket();
      tp->setMessageType(PacketType::START_CANCELED);
      tp->pStartCanceled.culprits.push_back(client->UID);
      tp->pStartCanceled.reason = StartCancelReason::ON_DEMAND;
      tp->pStartCanceled.sessionID = mID;
      mState = GAME_STATE_WAITING_FOR_START;
      broadcast(tp);
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Client \"" << client->CID << "\" sent a cancelStart packet but it is not permitted.";
         loggerptr->flushLogBuffer();
      }
      THROW_ERROR_CODE(OPERATION_NOT_PERMITED)
   }
}

void GameSession::doStartCheckings()
{
   if (!mAllShouldBeReady)
      startGame();

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::START_CANCELED);
   for (auto it : mIDtoReady)
      if (!it.second && mAllShouldBeReady)
         tp->pStartCanceled.culprits.push_back(getProfileFromCID(it.first)->UID);

   if (!tp->pStartCanceled.culprits.empty())
   {
      tp->pStartCanceled.reason = StartCancelReason::ALL_NOT_READY;
      tp->pStartCanceled.sessionID = mID;
      mState = GAME_STATE_WAITING_FOR_START;
      broadcast(tp);
   }
}

void GameSession::changeSide(GSProfilePtr client, clooponline::packet::ChangeSide& reqPacket, ThriftParser* repPacket)
{
   auto cs = client;
   if (reqPacket.__isset.asUID)
   {
      CHECK_IF_MANAGER(client, "change side")
      cs = getProfileFromUID(uint64_t(reqPacket.asUID));
   }

   CHECK_PARTICIPATION(cs, "change side")

   if (mState == GAME_STATE_INFANCY || mState == GAME_STATE_START_COUNTDOWN ||
       mState == GAME_STATE_LOCKED || (mState == GAME_STATE_PLAYING && !mPlayersCanChangeSideAfterStart && client != mManager))
   {
      THROW_ERROR_CODE(INVALID_SESSION_STATE)
   }

   if (reqPacket.newSide != 0)
   {
      auto it = mSidetoPlayerNum.find(reqPacket.newSide);
      if (it == mSidetoPlayerNum.end() || it->second >= mSidetoMaxPlayers[reqPacket.newSide])
      {
         THROW_ERROR_CODE(SIDE_NOT_AVAILABLE)
      }
   }

   auto sid = mIDtoSide.find(cs->CID);
   --mSidetoPlayerNum[sid->second];
   sid->second = uint(reqPacket.newSide);
   ++mSidetoPlayerNum[reqPacket.newSide];

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::SIDE_CHANGED);
   tp->pSideChanged.__set_sessionID(mID);
   tp->pSideChanged.__set_UID(cs->UID);
   tp->pSideChanged.__set_newSide(reqPacket.newSide);
   broadcast(tp, client->CID);
}

void
GameSession::setReady(GSProfilePtr client, clooponline::packet::SetReady& reqPacket, ThriftParser* repPacket)
{
   CHECK_STATE(client, GAME_STATE_START_COUNTDOWN, "set ready")
   CHECK_PARTICIPATION(client, "set ready")

   if (mIDtoReady[client->CID])
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_WARNING))
      {
         *loggerptr << "Client \"" << client->CID << "\" sent "
                    << "ready message to game session \"" << mID << "\". But "
                    << "he has already stated ready. Request skipped.";
         loggerptr->flushLogBuffer();
      }

      THROW_ERROR_CODE(ALREADY_STATED_READY)
   }

   mIDtoReady[client->CID] = true;

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Client \"" << client->CID << "\" has stated ready in session " << mID;
      loggerptr->flushLogBuffer();
   }

   for (auto& rit : mIDtoReady)
      if (!rit.second)
         return;

   mGameServer->timeoutDone(mStartTimerIndex);
   startGame();
}

void GameSession::managerKick(GSProfilePtr client, clooponline::packet::ManagerKick& reqPacket, ThriftParser* repPacket)
{
   CHECK_IF_MANAGER(client, "manager kick")
   CHECK_EVER_PARTICIPATED(reqPacket.UID, "manager kick")

   auto uid = reqPacket.UID;
   for (auto& t : mTempLefts)
   {
      if (t->UID == uid)
      {
         leftSession(t, LeavingReason::KICK);
         return;
      }
   }

   for (auto& t : mParticipants)
   {
      if (t->UID == uid)
      {
         leftSession(t, LeavingReason::KICK);
         return;
      }
   }
}

void GameSession::tempLeave(GSProfilePtr client, clooponline::packet::TemporaryLeave& reqPacket, ThriftParser* repPacket)
{
   CHECK_PARTICIPATION(client, "tempLeave")

   if (mState == GAME_STATE_PLAYING || mState == GAME_STATE_LOCKED)
   {
      tempLeftSession(client);
   }
   else
   {
      leftSession(client, LeavingReason::ON_DEMAND);
   }
}

void
GameSession::handleActiveSession(GSProfilePtr client, clooponline::packet::HandleActiveSession& reqPacket, ThriftParser* repPacket)
{
   if (client->AppID != mAppID)
      THROW_ERROR_CODE(INVALID_AID);

   GSProfilePtr preclient = 0;
   for (const auto& tl : mTempLefts)
   {
      if (tl->UID == client->UID)
         preclient = tl;
   }
   if (preclient->CID == 0)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Client " << client->CID
                    << " did not temporarily leave session \"" << mID << "\" but packet \""
                    << "handle active session" << "\" was given for him.";
         loggerptr->flushLogBuffer();
      }
      THROW_ERROR_CODE(NOT_DISCONNECTED)
   }

   repPacket->setMessageType(PacketType::HANDLE_ACTIVE_SESSION_RESPONSE);
   bool willBeManager = false;
   if (reqPacket.action == ActiveSessionActionType::JOIN)
   {
      if (mParticipants.empty()) willBeManager = true;
      if (preclient->CID != client->CID)
      {
         auto it2 = mIDtoSide.find(preclient->CID);
         std::swap(mIDtoSide[client->CID], it2->second);
         mIDtoSide.erase(it2);
         auto it3 = mIDtoReady.find(preclient->CID);
         std::swap(mIDtoReady[client->CID], it3->second);
         mIDtoReady.erase(it3);
         mParticipants.push_back(client);
      }
      mGameServer->joinedSession(client, mID);

      fillJoinSessionObject(repPacket->pHandleActiveSessionResponse.joinObject, uint(reqPacket.lastCachedRPCIndex),
                            uint(reqPacket.lastPropertiesRevisionIndex));
      mGameServer->sendPacket(client, repPacket);

      if (willBeManager)
         assignManager(client);

      auto tp = (ThriftParser*)mGameServer->createPacket();
      tp->setMessageType(PacketType::REJOINED);
      tp->pRejoined.__set_sessionID(int(mID));
      tp->pRejoined.__set_UID(client->UID);
      broadcast(tp, client->CID);
   }
   else
   {
      leftSession(preclient, LeavingReason::ON_DEMAND);
   }

   DCHandled(client);
}

void GameSession::changeConfigs(GSProfilePtr client, clooponline::packet::ChangeConfigs reqPacket, ThriftParser* repPacket)
{
   CHECK_IF_MANAGER(client, "change configs")

   if (mState == GAME_STATE_INFANCY || mState == GAME_STATE_START_COUNTDOWN || mState == GAME_STATE_LOCKED)
   {
      THROW_ERROR_CODE(INVALID_SESSION_STATE)
   }

   if (reqPacket.configs.__isset.sideToCapacity)
   {
      vector<uint> kickList;
      uint newMaxPlayers = 0;
      for (auto s : reqPacket.configs.sideToCapacity)
      {
         auto it = mSidetoPlayerNum.find(uint(s.first));
         if (it != mSidetoPlayerNum.end())
         {
            int kickNum = it->second - uint(s.second);
            for (auto pit = mIDtoSide.begin(); pit != mIDtoSide.end() && kickNum > 0; ++pit)
            {
               if (pit->second == uint(s.first))
               {
                  uint64_t uid = getProfileFromCID(pit->first)->UID;
                  if (reqPacket.configs.participantsToSide[uid] != uint(s.first))
                  {
                     kickList.push_back(pit->first);
                     --kickNum;
                  }
               }
            }

            if (kickNum > 0)
            {
               THROW_ERROR_CODE(INVALID_RESTRICTIONS);
            }
         }

         newMaxPlayers += s.second;
      }

      int kickNum = mMaxAllowedPlayers - newMaxPlayers;
      for (auto k : kickList)
      {
         if (kickNum > 0)
         {
            leftSession(getProfileFromCID(k), LeavingReason::KICK);
            --kickNum;
         }
         else
         {
            --mSidetoPlayerNum[mIDtoSide[k]];
            ++mSidetoPlayerNum[0];
            mIDtoSide[k] = 0;
         }
      }

      for (auto s : reqPacket.configs.sideToCapacity)
      {
         defineSide(uint(s.first), uint(s.second));
      }

      for (auto s : mSidetoPlayerNum)
      {
         if (reqPacket.configs.sideToCapacity.find(s.first) == reqPacket.configs.sideToCapacity.end())
         {
            mSidetoPlayerNum.erase(s.first);
            mSidetoMaxPlayers.erase(s.first);
         }
      }

      if (mState == GAME_STATE_PLAYING)
         fillDCSides();

      reqPacket.configs.__isset.sideToCapacity = true;
      for (auto& s : mSidetoMaxPlayers)
      {
         reqPacket.configs.sideToCapacity[int(s.first)] = int(s.second);
      }

      reqPacket.configs.__isset.participantsToSide = true;
      for (auto& p : mIDtoSide)
      {
         uint64_t uid = 0;
         for (auto& t : mTempLefts)
            if (t->CID == p.first)
            {
               uid = t->UID;
               break;
            }

         if (uid == 0)
            uid = getProfileFromCID(p.first)->UID;
         reqPacket.configs.participantsToSide[int64_t(uid)] = int(p.second);
      }
   }

   if (reqPacket.configs.__isset.alwaysAcceptJoin)
   {
      mAlwaysAcceptSubscription = reqPacket.configs.alwaysAcceptJoin;
   }

   if (reqPacket.configs.__isset.canChangeSideAfterStart)
   {
      mPlayersCanChangeSideAfterStart = reqPacket.configs.canChangeSideAfterStart;
   }

   if (reqPacket.configs.__isset.disconnectionBehavior)
   {
      mDCBehavior = reqPacket.configs.disconnectionBehavior;
   }

   if (reqPacket.configs.__isset.disconnectionTimeout)
   {
      mDCTimeout = uint(reqPacket.configs.disconnectionTimeout);
   }

   if (reqPacket.configs.__isset.options)
   {
      for (auto& o : reqPacket.configs.options)
      {
         if (o.second.empty())
            mOptions.erase(o.first);
         else
            mOptions[o.first] = o.second;
      }
   }

   if (reqPacket.configs.__isset.label)
   {
      mLabel = reqPacket.configs.label;
   }

   if (reqPacket.configs.__isset.password)
   {
      mPassword = reqPacket.configs.password;
   }

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::CONFIG_CHANGED);
   tp->pConfigsChanged.__set_sessionID(mID);
   tp->pConfigsChanged.__set_configs(reqPacket.configs);
   broadcast(tp);
}

void GameSession::leftSession(GSProfilePtr client, LeavingReason::type reason)
{
   CHECK_EVER_PARTICIPATED(client->UID, "left session")

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Client \"" << client->CID << "\" left session \"" << mID << "\".";
      loggerptr->flushLogBuffer();
   }

   if (mState != GAME_STATE_INFANCY)
   {
      auto pp = (ThriftParser*)mGameServer->createPacket();
      pp->setMessageType(PacketType::LEFT);
      pp->pLeft.__set_UID(client->UID);
      pp->pLeft.__set_sessionID(mID);
      pp->pLeft.__set_reason(reason);

      if (reason == LeavingReason::KICK && client->IsConnected)
         broadcast(pp);
      else
         broadcast(pp, client->CID);
   }

   if (mState == GAME_STATE_START_COUNTDOWN)
   {
      mGameServer->timeoutDone(mStartTimerIndex);
      auto tp = (ThriftParser*)mGameServer->createPacket();
      tp->setMessageType(PacketType::START_CANCELED);
      tp->pStartCanceled.culprits.push_back(client->UID);
      tp->pStartCanceled.reason = StartCancelReason::SOMEONE_LEFT;
      tp->pStartCanceled.sessionID = mID;
      mState = GAME_STATE_WAITING_FOR_START;
      broadcast(tp, client->CID);
   }

   uint side = mIDtoSide[client->CID];
   --mSidetoPlayerNum[side];

   mIDtoSide.erase(client->CID);
   mIDtoReady.erase(client->CID);
   for (auto p = mParticipants.begin(); p != mParticipants.end(); ++p)
   {
      if ((*p)->CID == client->CID)
      {
         mParticipants.erase(p);
         break;
      }
   }

   mGameServer->clientLeftSession(client, mID);
   if (mParticipants.empty() && mTempLefts.empty())
   {
      endSession();
      return;
   }

   if (client == mManager)
      assignManager();
}

void GameSession::tempLeftSession(GSProfilePtr client)
{
   auto it = std::find(mTempLefts.begin(), mTempLefts.end(), client);
   if (it == mTempLefts.end())
   {
      mTempLefts.push_back(client);
      mGameServer->defineTempLeftSession(client, mID);
      mParticipants.erase(find(mParticipants.begin(), mParticipants.end(), client));
   }

   auto pp = (ThriftParser*)mGameServer->createPacket();
   pp->setMessageType(PacketType::TEMP_LEFT);
   pp->pTemporaryLeft.__set_sessionID(mID);
   pp->pTemporaryLeft.__set_UID(client->UID);
   broadcast(pp);

   if (client == mManager)
   {
      assignManager();
   }
}

void GameSession::fillDCSides()
{
   for (auto& it : mIDtoSide)
   {
      if (it.second == 0)
      {
         size_t s;
         map<uint, uint>::iterator numit;
         map<uint, uint>::iterator maxit;

         do {
            s = rand() % mSidetoMaxPlayers.size();
            maxit = next(mSidetoMaxPlayers.begin(), s);
            numit = mSidetoPlayerNum.find(maxit->first);
            if (numit->second < maxit->second || maxit->second == 0)
               break;
         } while (true);

         it.second = maxit->first;
         ++(numit->second);
      }
   }

   mSidetoPlayerNum[0] = 0;
}

void GameSession::startGame()
{
   fillDCSides();
   mState = GAME_STATE_PLAYING;

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::STARTED);
   tp->pStarted.__set_sessionID(mID);
   tp->pStarted.__set_serverTime(long(mGameServer->mThreadPool.getTime()));

   for (auto& p : mIDtoSide)
   {
      tp->pStarted.UIDToSideMap[getProfileFromCID(p.first)->UID] = p.second;
   }

   broadcast(tp);
}

void GameSession::broadcast(PacketParser* msg, uint exceptionID)
{
   for (auto& p : mParticipants)
   {
      if (p->CID != exceptionID)
         mGameServer->sendPacket(p, msg);
   }
}

bool GameSession::filter(std::map<std::string, std::string> params)
{
   //check params
   for (auto& par : params)
   {
      auto p = mOptions.find(par.first);
      if (p == mOptions.end() || p->second != par.second)
         return false;
   }

   return true;
}

bool GameSession::canJoin(GSProfilePtr client, const clooponline::packet::JoinSession& joinParams)
{
   if (mAppID != client->AppID)
   {
      return false;
   }

   if (find(mJoinedSoFar.begin(), mJoinedSoFar.end(), client->UID) != mJoinedSoFar.end())
      return false;

   if (!(mAlwaysAcceptSubscription || mState == GAME_STATE_WAITING_FOR_START))
      return false;

   if (mParticipants.size() >= mMaxAllowedPlayers)
      return false;

   if (joinParams.side != 0)
   {
      auto numit = mSidetoPlayerNum.find(joinParams.side);
      auto maxit = mSidetoMaxPlayers.find(joinParams.side);
      if (numit == mSidetoPlayerNum.end())
         return false;

      if (numit->second >= maxit->second && maxit->second != 0)
         return false;
   }

   return (joinParams.password == mPassword);
}

void GameSession::joinPlayer(GSProfilePtr client, const clooponline::packet::JoinSession& joinParams, ThriftParser* repPacket)
{
   addParticipant(client, uint(joinParams.side));
   repPacket->setMessageType(PacketType::JOIN_SESSION_RESPONSE);
   fillJoinSessionObject(repPacket->pJoinSessionResponse);
   mGameServer->sendPacket(client, repPacket);

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Clinet " << client->CID << " joined session \"" << mID << "\".";
      loggerptr->flushLogBuffer();
   }

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::JOINED);
   tp->pJoined.__set_UID(client->UID);
   tp->pJoined.__set_sessionID(mID);
   tp->pJoined.__set_side(mIDtoSide[client->CID]);
   broadcast(tp, client->CID);
}

void GameSession::joinPlayer(GSProfilePtr client, const clooponline::packet::Subscribe& subs)
{
   mAverageIndicator =
      ((mJoinedSoFar.size() * mAverageIndicator) + float(subs.indicator)) / (mJoinedSoFar.size() + 1);
   if (subs.__isset.maxIndicatorDistance && subs.maxIndicatorDistance < mMaxAllowedIndicatorDistance)
      mMaxAllowedIndicatorDistance = float(subs.maxIndicatorDistance);

   for (auto& o : subs.configs.options)
   {
      mOptions[o.first] = o.second;
   }

   addParticipant(client, uint(subs.side));
   auto side = mIDtoSide.find(client->CID);
   if (side == mIDtoSide.end())
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Session \"" << mID << "\" could not assign a side to client \"" << client->CID << "\".";
         loggerptr->flushLogBuffer();
      }

      throw runtime_error("internal error");
   }

   if (mState != GAME_STATE_INFANCY)
   {
      auto tp = (ThriftParser*)mGameServer->createPacket();
      tp->setMessageType(PacketType::JOINED);
      tp->pJoined.__set_UID(client->UID);
      tp->pJoined.__set_sessionID(mID);
      tp->pJoined.__set_side(side->second);
      broadcast(tp, client->CID);
   }
}

bool GameSession::canJoin(GSProfilePtr client, const clooponline::packet::Subscribe& subs)
{
   if (mAppID != client->AppID)
   {
      return false;
   }

   if (find(mJoinedSoFar.begin(), mJoinedSoFar.end(), client->UID) != mJoinedSoFar.end())
      return false;

   if (!(mAlwaysAcceptSubscription || mState == GAME_STATE_WAITING_FOR_START || mState == GAME_STATE_INFANCY))
      return false;

   if (mParticipants.size() >= mMaxAllowedPlayers)
      return false;

   if (subs.side != 0)
   {
      auto numit = mSidetoPlayerNum.find(subs.side);
      auto maxit = mSidetoMaxPlayers.find(subs.side);
      if (numit == mSidetoPlayerNum.end())
         return false;

      if (numit->second == maxit->second && maxit->second != 0)
         return false;
   }

   if (subs.configs.password != mPassword)
      return false;

   for (auto& o : subs.configs.options)
   {
      auto it = mOptions.find(o.first);
      if (it != mOptions.end() && it->second != o.second)
         return false;
   }

   return (abs(subs.indicator - mAverageIndicator) <= min(float(subs.maxIndicatorDistance), mMaxAllowedIndicatorDistance));
}

void
GameSession::RPCCall(GSProfilePtr client, clooponline::packet::RPCCall& reqPacket, ThriftParser* repPacket)
{
   CHECK_PARTICIPATION(client, "RPCCall")
   CHECK_NOT_STATE(client, GAME_STATE_INFANCY, "RPCCall")
   CHECK_NOT_STATE(client, GAME_STATE_LOCKED, "RPCCall")

   vector<uint> recepts;
   for (auto& r : reqPacket.receptors)
   {
      auto p = getProfileFromUID(uint64_t(r));
      CHECK_PARTICIPATION(p, "RPCCallReceptor")
      recepts.push_back(p->CID);
   }

   ThriftParser* rc;
   if (reqPacket.__isset.asUID)
   {
      CHECK_IF_MANAGER(client, "RPCCall")
      CHECK_PARTICIPATION(getProfileFromUID(uint64_t(reqPacket.asUID)), "RPCCallAs");
      rc = (ThriftParser*)mGameServer->createPacket();
      rc->pRPCCalled.UID = reqPacket.asUID;
   }
   else
   {
      rc = (ThriftParser*)mGameServer->createPacket();
      rc->pRPCCalled.UID = client->UID;
   }
   rc->setMessageType(PacketType::RPC_CALLED);

   uint ind = ++mLastRPCIndex;
   rc->pRPCCalled.__set_sessionID(mID);
   rc->pRPCCalled.__set_RPCindex(ind);
   rc->pRPCCalled.__set_methodname(reqPacket.methodname);
   rc->pRPCCalled.__set_parameters(reqPacket.parameters);
   rc->pRPCCalled.__set_serverTime(long(mGameServer->mThreadPool.getTime()));

   if (!(reqPacket.__isset.receptors))
   {
      broadcast(rc, client->CID);
   }
   else if (reqPacket.receptors.empty())
   {
      broadcast(rc);
   }
   else
   {
      for (auto& r : recepts)
      {
         mGameServer->sendPacket(r, rc);
      }
   }

   if (reqPacket.cached)
      mChachedRPCs.push_back(move(*rc));

   repPacket->setMessageType(PacketType::RPC_CALL_RESPONSE);
   repPacket->pRPCCallResponse.__set_RPCindex(ind);
   repPacket->pRPCCallResponse.__set_serverTime(rc->pRPCCalled.serverTime);
   mGameServer->sendPacket(client, repPacket);
}

void GameSession::timeoutDone(ulong timerind)
{
   mGameServer->timeoutDone(timerind);
   auto it = mPendingTimerIndices.find(timerind);
   if (it != mPendingTimerIndices.end())
      mPendingTimerIndices.erase(it);
}

void GameSession::terminate(GSProfilePtr client, clooponline::packet::Terminate& reqPacket, ThriftParser* repPacket)
{
   CHECK_IF_MANAGER(client, "terminate")

   auto tp = (ThriftParser*)mGameServer->createPacket();
   tp->setMessageType(PacketType::TERMINATED);
   tp->pTerminated.__set_sessionID(mID);
   tp->pTerminated.__set_reason(TerminationReason::ON_DEMAND);
   broadcast(tp);
   endSession();
}

void GameSession::DCHandled(GSProfilePtr client)
{
   auto preit = mTempLefts.begin();
   for (; preit != mTempLefts.end() && (*preit)->UID != client->UID; ++preit) ;

   if (preit != mTempLefts.end())
   {
      //clean the reconnection time event
      auto tit = mDCClientIDtoTimerInd.find((*preit)->CID);
      mTempLefts.erase(preit);
      if (tit != mDCClientIDtoTimerInd.end())
      {
         timeoutDone(tit->second);
         mDCClientIDtoTimerInd.erase(tit);
      }

      if (mTempLefts.empty() && mState == GAME_STATE_LOCKED)
      {
         auto pp = (ThriftParser*)mGameServer->createPacket();
         pp->setMessageType(PacketType::RESUMED);
         pp->pResumed.__set_sessionID(mID);
         broadcast(pp);
         mState = GAME_STATE_PLAYING;
      }

      mGameServer->removeTempLeftSession(client, mID);

      if (mParticipants.empty() && mTempLefts.size() == 0)
      {
         endSession();
         return;
      }
   }
}

void GameSession::leaveSession(GSProfilePtr client)
{
   leftSession(std::move(client), LeavingReason::ON_DEMAND);
}

float GameSession::getAverageIndicator()
{
   return mAverageIndicator;
}

void GameSession::promoteToActiveSession(uint ID)
{
   mState = GAME_STATE_WAITING_FOR_START;
   mID = ID;
   ++LiveSessionsCount;
   assignManager();

   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Session " << ID << " promoted to host a matched session.";
      loggerptr->flushLogBuffer();
   }
}

void GameSession::fillSessionConfigs(clooponline::packet::SessionConfigs& sc)
{
   //TODO: why not save a clooponline::packet::SessionConfigs object in session and always work with that?!
   sc.__isset.sideToCapacity = true;
   for (auto& s : mSidetoMaxPlayers)
   {
      sc.sideToCapacity[int(s.first)] = int(s.second);
   }

   sc.__isset.participantsToSide = true;
   for (auto& p : mIDtoSide)
   {
      uint64_t uid = getProfileFromCID(p.first)->UID;
      sc.participantsToSide[int64_t(uid)] = int(p.second);
   }

   sc.__isset.tempLeftParticipants = true;
   for (auto& p : mTempLefts)
   {
      sc.tempLeftParticipants.push_back(int64_t(p->UID));
   }

   sc.__set_propertyRevisionIndex(mPropertiesRevisionIndex);
   sc.__set_lastRPCIndex(mLastRPCIndex);
   sc.__set_alwaysAcceptJoin(mAlwaysAcceptSubscription);
   sc.__set_canChangeSideAfterStart(mPlayersCanChangeSideAfterStart);
   sc.__set_disconnectionBehavior(mDCBehavior);
   sc.__set_disconnectionTimeout(int(mDCTimeout));

   sc.__isset.options = true;
   for (auto& o : mOptions)
   {
      sc.options.insert(o);
   }

   if (!mLabel.empty())
      sc.__set_label(mLabel);

   switch (mState)
   {
      case GAME_STATE_INFANCY:
      case GAME_STATE_WAITING_FOR_START:
      case GAME_STATE_START_COUNTDOWN:
         sc.__set_isStarted(false);
         break;
      case GAME_STATE_PLAYING:
      case GAME_STATE_LOCKED:
      case GAME_STATE_ENDED:
         sc.__set_isStarted(true);
         break;
   }
}

void GameSession::fillJoinSessionObject(clooponline::packet::JoinSessionResponse& joinObject, uint rpcFromIndex, uint propertiesFromIndex)
{
   fillSessionConfigs(joinObject.configs);
   joinObject.__set_serverTime(long(mGameServer->mThreadPool.getTime()));
   if (propertiesFromIndex < mPropertiesRevisionIndex)
   {
      joinObject.__isset.properties = true;
      joinObject.properties = mProperties;
   }

   if (!mChachedRPCs.empty())
      for (size_t i = mChachedRPCs.size() - 1; i >= 0; --i)
      {
         if (rpcFromIndex < mChachedRPCs[i].pRPCCalled.RPCindex)
         {
            joinObject.cachedRPCs.push_back(mChachedRPCs[i].pRPCCalled);
            joinObject.__isset.cachedRPCs = true;

            if (i == 0)
               break;
         }
         else
            break;
      }
}

GSProfilePtr GameSession::getProfileFromCID(uint CID)
{
   for (auto& p : mParticipants)
      if (p->CID == CID)
         return p;

   for (auto& t : mTempLefts)
      if (t->CID == CID)
         return t;

   auto re = make_shared<GSProfile>(0);
   re->UID = 0;
   re->AppID = 0;
   return re;
}

GSProfilePtr GameSession::getProfileFromUID(uint64_t UID)
{
   for (auto& p : mParticipants)
      if (p->UID == UID)
         return p;

   for (auto& t : mTempLefts)
      if (t->UID == UID)
         return t;

   auto re = make_shared<GSProfile>(0);
   re->UID = 0;
   re->AppID = 0;
   return re;
}