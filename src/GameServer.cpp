#include <GameServer.h>
#include <MonitoringManager.h>
#include <Singleton.h>
#include <Logger.h>
#include <WS_TCP_Provider.h>
#include <Socket_TCP_Provider.h>
#include <GSAAAChecker.h>
#include <ThriftParser.h>
#include <ThriftException.h>

#define CHECK_IF_MANAGER(PACKET_NAME) \
   if (!(gs->isManager(clientID))) \
   { \
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR)) \
      { \
         *loggerptr << "Client " << clientID \
                    << " sent a \"" << PACKET_NAME << "\" packet but was not assigned to be the manager of that session."; \
         loggerptr->flushLogBuffer(); \
      } \
      THROW_ERROR_CODE(NOT_MANAGER) \
   }

#define ADD_WRAPPED_JOB(FUNC) \
   function<void()> f = bind(&GameSession::FUNC, gs, gsclient, innerpack, trepPacket); \
   mThreadPool.AddJob(gs->getID(), &Router::callWrapper, (Router*)this, f, gsclient, reqPacket, repPacket);

#define UPDATE_PROFILE(CLIENT) \
    GSProfilePtr gsclient = static_pointer_cast<const GSProfile>(CLIENT);

using namespace std;
using namespace clooponline::packet;

extern shared_ptr<Logger> loggerptr;
extern shared_ptr<MonitoringManager> monitorptr;

GameServer::GameServer(uint16_t wsPort, uint16_t socketPort, string loggeraddr, std::string aaaip, uint16_t aaaport,
                       string aaaurlpath, string gstoken, ushort threadsNum)
                  : Router(threadsNum),
                    mSubscribeByCID(mSubscribePool.get<SubsInfo::ByCID>()),
                    mSubscribeByTID(mSubscribePool.get<SubsInfo::ByTID>())
{
   mLastGameSessionID = 1;
   mLastSubsTrackID = 0;
   setTypeHandler(PacketType::CREATE_SESSION, bind(&GameServer::createSession, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::JOIN_SESSION, bind(&GameServer::joinSession, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::SUBSCRIBE, bind(&GameServer::subscribe, this, _1, _2, _3), 1), 1;
   setTypeHandler(PacketType::UNSUBSCRIBE, bind(&GameServer::unsubscribe, this, _1, _2, _3), 1);

   setTypeHandler(PacketType::CHANGE_SIDE, bind(&GameServer::changeSide, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::CHANGE_CONFIGS, bind(&GameServer::changeConfigs, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::LEAVE_SESSION, bind(&GameServer::leaveSession, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::TEMP_LEAVE, bind(&GameServer::tempLeave, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::SESSION_LIST, bind(&GameServer::sessionList, this, _1, _2, _3), 1);

   setTypeHandler(PacketType::RPC_CALL, bind(&GameServer::clientRPCCall, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::SET_READY, bind(&GameServer::setReady, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::START, bind(&GameServer::startSession, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::CANCEL_START, bind(&GameServer::cancelStartSession, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::SET_PROPERTIES, bind(&GameServer::setSessionProperties, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::GET_PROPERTIES, bind(&GameServer::getSessionProperties, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::TERMINATE, bind(&GameServer::terminateSession, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::MANAGER_KICK, bind(&GameServer::managerKick, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::GET_AVAILABLE_ACTIVE_SESSIONS, bind(&GameServer::getActiveSessions, this, _1, _2, _3), 1);
   setTypeHandler(PacketType::HANDLE_ACTIVE_SESSION, bind(&GameServer::handleActiveSession, this, _1, _2, _3), 1);

   setDCHandler(bind(&GameServer::onUserDC, this, _1), 1);
   setIdentifyHandler(bind(&GameServer::onUserConnect, this, _1), 1);

   addNetProvider<WS_TCP_Provider>(wsPort, true, string("gameserver"));
   addNetProvider<Socket_TCP_Provider>(socketPort, true, string("gameserver"));
   addAAAChecker<GSAAAChecker>(aaaip, aaaport, aaaurlpath, gstoken);
   mGSAAAChecker = (GSAAAChecker*) mAAAChecker.get();

   monitorptr->addMonitoringVariable("gameserver.session_created", &mSessionCreatedCount);
   monitorptr->addMonitoringVariable("gameserver.session_started", &mSessionStartedCount);
   monitorptr->addMonitoringVariable("gameserver.session_ended", &mSessionEndedCount);
   monitorptr->addMonitoringVariable("gameserver.handled_active_session", &mHandledDCCount);
   monitorptr->addMonitoringVariable("gameserver.joined_session", &mJoinedSessionCount);
   monitorptr->addMonitoringVariable("gameserver.queued_jobs", &mQueuedJobs);
   monitorptr->addMonitoringVariable("gameserver.live_sessions", &mLiveSessions);
   monitorptr->addIterationNotifyee(bind(&GameServer::monitoringIterated, this));
}

GameServer::~GameServer()
{
   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Game server terminated.";
      loggerptr->flushLogBuffer();
   }
}

void GameServer::cleanShutdown()
{
   Router::cleanShutdown();
   if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
   {
      *loggerptr << "Game server shut down.";
      loggerptr->flushLogBuffer();
   }
}

void GameServer::monitoringIterated()
{
   mSessionCreatedCount = mSessionStartedCount = mSessionEndedCount =
   mHandledDCCount = mJoinedSessionCount = 0;
   mQueuedJobs = uint(mThreadPool.getPendingJobsCount());
   mLiveSessions = GameSession::getLiveSessionsCount();
}

void GameServer::onUserConnect(ProfilePtr client)
{
   UPDATE_PROFILE(client)
   auto pp = (ThriftParser*)createPacket();
   fillActiveSessions(gsclient, pp);
   if (pp->pActiveSessions.sessionIDToConfigs.size())
      sendPacket(client, pp);
}

void GameServer::fillActiveSessions(GSProfilePtr client, ThriftParser* packet)
{
   packet->setMessageType(PacketType::AVAILABLE_ACTIVE_SESSIONS);
   auto sessions = getTempLeftSessions(client);

   if (!sessions.empty())
   {
      for (auto s : sessions)
      {
         if (s->getAppID() == client->AppID)
            s->fillSessionConfigs(packet->pActiveSessions.sessionIDToConfigs[int(s->getID())]);
      }
   }
}

void GameServer::defineTempLeftSession(GSProfilePtr client, uint sid)
{
   mThreadPool.AddJob(1, [this, client, sid]{
      mTempLeftMap.insert(make_pair(client->UID, getGameSession(sid)));
   });
}

void GameServer::removeTempLeftSession(GSProfilePtr client, uint sid)
{
   mThreadPool.AddJob(1, [client, sid, this](){
      auto sessions = mTempLeftMap.equal_range(client->UID);

      for (auto it = sessions.first; it != sessions.second; ++it)
      {
         if (it->second->getID() == sid)
         {
            mTempLeftMap.erase(it);
            break;
         }
      }
   });
}

uint GameServer::generateNextGameSessionID()
{
   uint sid = ++mLastGameSessionID;
   return (sid) ? sid : ++++mLastGameSessionID;
}

void GameServer::onUserDC(ProfilePtr client)
{
   UPDATE_PROFILE(client)
   auto sessions = getGameSessions(gsclient);

   for (auto gs : sessions)
      mThreadPool.AddJob(gs->getID(), bind(&GameSession::userDisconnected, gs, gsclient));

   mIDtoSessions.erase(client->CID);

   auto it = mSubscribeByCID.find(client->CID);
   if (it != mSubscribeByCID.end())
   {
      mThreadPool.RemoveEvent(it->TimeEventID);
      for (auto sit : it->PossibleSessions)
      {
         sit->leaveSession(gsclient);
      }
      mSubscribeByCID.erase(it);
   }
}

void GameServer::getActiveSessions(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   auto pp = (ThriftParser*)createPacket();
   fillActiveSessions(gsclient, pp);
   sendPacket(client, pp);
}

void GameServer::handleActiveSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(HandleActiveSession)
   FIND_GAME_SESSION("handle active session")
   ADD_WRAPPED_JOB(handleActiveSession)
}

void GameServer::setSessionProperties(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(SetProperties)
   FIND_GAME_SESSION("set properties")
   ADD_WRAPPED_JOB(setProperties)
}

void GameServer::getSessionProperties(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(GetProperties)
   FIND_GAME_SESSION("get properties")
   ADD_WRAPPED_JOB(getProperties)
}

void GameServer::managerKick(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(ManagerKick)
   FIND_GAME_SESSION("manager kick")
   ADD_WRAPPED_JOB(managerKick)
}

void GameServer::createSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(CreateSession)

   uint sid = generateNextGameSessionID();
   trepPacket->setMessageType(clooponline::packet::PacketType::CREATE_SESSION_RESPONSE);
   trepPacket->pCreateSessionResponse.__set_sessionID(sid);
   trepPacket->pCreateSessionResponse.__set_serverTime(long(mThreadPool.getTime()));
   sendPacket(client, repPacket);

   shared_ptr <GameSession> gs = make_shared<GameSession>(this, sid, static_pointer_cast<const GSProfile>(client),
                                                          gsclient->AppID, innerpack);
   mManualSessions.insert(make_pair(sid, gs));
   mIDtoSessions.insert(make_pair(client->CID, gs));
}

void GameServer::joinSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(JoinSession)

   auto gs = getGameSession(uint(innerpack.sessionID));
   if (gs)
   {
      bool joinedFlag = false;
      mThreadPool.WaitFor(mThreadPool.AddJob(gs->getID(), [&gs, &joinedFlag, &innerpack, &gsclient, &treqPacket, &trepPacket](){
         if (gs->canJoin(gsclient, innerpack))
         {
            gs->joinPlayer(gsclient, innerpack, trepPacket);
            joinedFlag = true;
         }
      }));

      if (joinedFlag)
      {
         mIDtoSessions.insert(make_pair(client->CID, gs));
      }
      else
      {
         THROW_ERROR_CODE(WRONG_OPTIONS)
      }
   }
   else
   {
      THROW_ERROR_CODE(INVALID_SID)
   }
}

void GameServer::subscribe(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(Subscribe)

   SubsInfo si;
   si.CID = client->CID;
   si.TID = ++mLastSubsTrackID;
   si.Configs = innerpack;
   if (si.Configs.maxIndicatorDistance == 0)
      si.Configs.maxIndicatorDistance = numeric_limits<double>::max();
   trepPacket->setMessageType(clooponline::packet::PacketType::SUBSCRIBE_RESPONSE);
   trepPacket->pSubscribeResponse.__set_trackID(si.TID);
   sendPacket(client, repPacket);

   for (auto it = mSubscribeByCID.begin(); it != mSubscribeByCID.end(); ++it)
   {
      auto gsptr = make_shared<GameSession>(this, static_pointer_cast<const GSProfile>(getProfile(it->CID)), gsclient->AppID, it->Configs);
      if (gsptr->canJoin(gsclient, si.Configs))
      {
         gsptr->joinPlayer(gsclient, si.Configs);
         bool canStart = gsptr->getPlayersNum() >= it->Configs.configs.minPlayerToMatch;
         for (auto git : it->PossibleSessions)
         {
            if (git->canJoin(gsclient, si.Configs))
            {
               git->joinPlayer(gsclient, si.Configs);
               si.PossibleSessions.push_back(git);
               if (git->getPlayersNum() >= it->Configs.configs.minPlayerToMatch)
                  canStart = true;
            }
         }

         mSubscribeByCID.modify(it, SubsInfo::PushToPossibleSessions(gsptr));
         si.PossibleSessions.push_back(gsptr);

         if (it->IsOnPriority && canStart)
         {
            mSubscribePool.insert(std::move(si));
            subscriptionPoolTimedOut(it->TID);
            return;
         }
      }
   }

   auto subit = mSubscribePool.insert(std::move(si));
   if (subit.second)
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
      {
         *loggerptr << "Client " << client->CID << "'s subscription pooled.";
         loggerptr->flushLogBuffer();
      }

      if (subit.first->Configs.poolTime == 0)
         subscriptionPoolTimedOut(subit.first->TID);
      else
      {
         mSubscribePool.modify(subit.first, SubsInfo::ChangeTimeEventID(
            mThreadPool.AddEvent(1, bind(&GameServer::subscriptionPoolTimedOut, this, subit.first->TID), uint(subit.first->Configs.poolTime))
         ));
      }
   }
   else
   {
      if (loggerptr->lockBuffer(LOG_SEVERITY_ERROR))
      {
         *loggerptr << "Inserting Client " << client->CID << "'s subscription to pool failed.";
         loggerptr->flushLogBuffer();
      }
   }
}

void GameServer::unsubscribe(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(Unsubscribe)

   auto it = mSubscribeByTID.find(innerpack.trackID);
   if (it == mSubscribeByTID.end())
   {
      THROW_ERROR_CODE(INVALID_TID)
   }

   if (it->CID == client->CID)
   {
      mThreadPool.RemoveEvent(it->TimeEventID);
      for (auto sit : it->PossibleSessions)
      {
         sit->leaveSession(gsclient);
      }

      mSubscribeByTID.erase(it);
   }
   else
      THROW_ERROR_CODE(INVALID_TID)
}

void GameServer::tempLeave(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(TemporaryLeave)

   auto gs = getGameSession(uint(innerpack.sessionID));
   if (gs)
   {
      mThreadPool.AddJob(gs->getID(), bind(&GameSession::tempLeave, gs, gsclient, innerpack, trepPacket));
      auto subs = mIDtoSessions.equal_range(client->CID);
      for (auto it = subs.first; it != subs.second;)
      {
         if (it->second->getID() == uint(innerpack.sessionID))
         {
            mIDtoSessions.erase(it);
            return;
         }
      }
   }
   else
   {
      THROW_ERROR_CODE(INVALID_SID);
   }
}

void GameServer::sessionList(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(SessionList)
   trepPacket->setMessageType(PacketType::SESSION_LIST_RESPONES);

   map<string, string> params = innerpack.filterOptions;

   for (auto it = mManualSessions.begin(); it != mManualSessions.end(); ++it)
   {
      mThreadPool.WaitFor(mThreadPool.AddJob(it->second->getID(), [gsclient, &it, &trepPacket, &params](){
         if (it->second->getAppID() == gsclient->AppID && it->second->filter(params))
         {
            SessionConfigs cc;
            it->second->fillSessionConfigs(cc);
            trepPacket->pSessionListResponse.IDtoConfigs.insert({it->first, move(cc)});
         }
      }));
   }

   sendPacket(client, repPacket);
}

void GameServer::clientLeftSession(GSProfilePtr client, uint sid)
{
   mThreadPool.AddJob(1, [this, client, sid](){
      auto subs = mIDtoSessions.equal_range(client->CID);

      for (auto it = subs.first; it != subs.second; ++it)
      {
         if (it->second->getID() == sid)
         {
            mIDtoSessions.erase(it);
            return;
         }
      }
   });
}

void GameServer::joinedSession(GSProfilePtr client, uint sid)
{
   mThreadPool.AddJob(1, [this, client, sid](){
      if (mTempLeftMap.find(client->UID) != mTempLeftMap.end())
      {
         mIDtoSessions.insert(make_pair(client->CID, getGameSession(sid)));
      }
   });
}

shared_ptr<GameSession> GameServer::getGameSession(uint sid)
{
   auto it = mManualSessions.find(sid);
   auto re = shared_ptr<GameSession>();
   if (it != mManualSessions.end())
      re = it->second;
   else
   {
      it = mMatchedSessions.find(sid);
      if (it != mMatchedSessions.end())
         re = it->second;
   }
   return re;
}

std::vector<shared_ptr<GameSession>> GameServer::getGameSessions(GSProfilePtr client)
{
   std::vector<shared_ptr<GameSession>> re;
   auto sessions = mIDtoSessions.equal_range(client->CID);
   for (auto it = sessions.first; it != sessions.second; ++it)
   {
      re.push_back(it->second);
   }
   return re;
}

std::vector<shared_ptr<GameSession>> GameServer::getTempLeftSessions(GSProfilePtr client)
{
   std::vector<shared_ptr<GameSession>> re;
   auto sessions = mTempLeftMap.equal_range(client->UID);
   for (auto it = sessions.first; it != sessions.second; ++it)
   {
      re.push_back(it->second);
   }
   return re;
}

void GameServer::clientRPCCall(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(RPCCall)
   FIND_GAME_SESSION("RPCCall")
   ADD_WRAPPED_JOB(RPCCall)
}

void GameServer::setReady(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(SetReady)
   FIND_GAME_SESSION("set ready")
   ADD_WRAPPED_JOB(setReady)
}

void GameServer::startSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(Start)
   FIND_GAME_SESSION("start session")
   ADD_WRAPPED_JOB(startSession)
}

void GameServer::cancelStartSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(CancelStart)
   FIND_GAME_SESSION("start session")
   ADD_WRAPPED_JOB(cancelStartSession)
}

void GameServer::changeSide(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(ChangeSide)
   FIND_GAME_SESSION("change side")
   ADD_WRAPPED_JOB(changeSide)
}

void GameServer::changeConfigs(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(ChangeConfigs)
   FIND_GAME_SESSION("change configs")
   ADD_WRAPPED_JOB(changeConfigs)
}

void GameServer::leaveSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(LeaveSession)
   FIND_GAME_SESSION("leave session")
   function<void()> f = bind(&GameSession::leaveSession, gs, gsclient);
   mThreadPool.AddJob(gs->getID(), &Router::callWrapper, (Router*)this, f, gsclient, reqPacket, repPacket);
}

void GameServer::terminateSession(ProfilePtr client, PacketParser* reqPacket, PacketParser* repPacket)
{
   UPDATE_PROFILE(client)
   HANDLE_METHOD_HEAD(Terminate)
   FIND_GAME_SESSION("terminate session")
   ADD_WRAPPED_JOB(terminate)
}

void GameServer::endSession(uint sid)
{
   mThreadPool.AddJob(1, [this, sid](){
      mThreadPool.jobGroupFinished(sid);
      mManualSessions.erase(sid);
      mMatchedSessions.erase(sid);
   });
}

void GameServer::subscriptionPoolTimedOut(uint64_t TID)
{
   auto subs = mSubscribeByTID.find(TID);
   if (subs == mSubscribeByTID.end())
      return;

   {
      float minDist = numeric_limits<float>::max();
      auto bestSession = mMatchedSessions.end();
      for (auto it = mMatchedSessions.begin(); it != mMatchedSessions.end(); ++it)
      {
         mThreadPool.WaitFor(mThreadPool.AddJob(it->second->getID(), [&it, &subs, &minDist, &bestSession, this](){
            if (abs(it->second->getAverageIndicator() - subs->Configs.indicator) < minDist &&
                it->second->canJoin(static_pointer_cast<const GSProfile>(getProfile(subs->CID)), subs->Configs))
            {
               minDist = float(abs(it->second->getAverageIndicator() - subs->Configs.indicator));
               bestSession = it;
            }
         }));
      }

      if (bestSession != mMatchedSessions.end())
      {
         mThreadPool.WaitFor(mThreadPool.AddJob(bestSession->second->getID(), [this, &bestSession, &subs, &minDist](){
            bestSession->second->joinPlayer(static_pointer_cast<const GSProfile>(getProfile(subs->CID)), subs->Configs);
            auto sl = (ThriftParser*)createPacket();
            sl->setMessageType(PacketType::SUBSCRIPTION_RESULT);
            sl->pSubscriptionResult.__set_sessionID(bestSession->second->getID());
            sl->pSubscriptionResult.__set_trackID(subs->TID);
            bestSession->second->fillJoinSessionObject(sl->pSubscriptionResult.joinObject);
            sendPacket(subs->CID, sl);
         }));
         mIDtoSessions.insert(make_pair(subs->CID, bestSession->second));
         return;
      }
   }

   float minDist = numeric_limits<float>::max();
   auto bestSession = subs->PossibleSessions.end();
   for (auto ps = subs->PossibleSessions.begin(); ps != subs->PossibleSessions.end(); ++ps)
   {
      if ((*ps)->getPlayersNum() >= subs->Configs.configs.minPlayerToMatch &&
          abs((*ps)->getAverageIndicator() - subs->Configs.indicator) < minDist)
      {
         minDist = float(abs((*ps)->getAverageIndicator() - subs->Configs.indicator));
         bestSession = ps;
      }
   }

   if (bestSession != subs->PossibleSessions.end())
   {
      promoteSession(*bestSession);
   }
   else
   {
      mSubscribeByTID.modify(subs, SubsInfo::ChangePriority(true));
   }
}

void GameServer::promoteSession(std::shared_ptr<GameSession> gsptr)
{
   uint sid = generateNextGameSessionID();
   mMatchedSessions.insert(make_pair(sid, gsptr));
   auto sl = (ThriftParser*)createPacket();
   sl->setMessageType(PacketType::SUBSCRIPTION_RESULT);
   sl->pSubscriptionResult.__set_sessionID(sid);
   gsptr->fillJoinSessionObject(sl->pSubscriptionResult.joinObject);

   for (auto p : gsptr->getParticipants())
   {
      auto sit = mSubscribeByCID.find(p->CID);
      if (sit != mSubscribeByCID.end())
      {
         mThreadPool.RemoveEvent(sit->TimeEventID);
         for (auto s : sit->PossibleSessions)
         {
            if (s != gsptr)
            {
               s->leaveSession(p);
            }
         }

         sl->pSubscriptionResult.__set_trackID(sit->TID);
         sendPacket(p, sl);
         mIDtoSessions.insert(make_pair(p->CID, gsptr));
         mSubscribeByCID.erase(sit);
      }
   }

   gsptr->promoteToActiveSession(sid);
}
