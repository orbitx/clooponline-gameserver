#include <GSAAAChecker.h>
#include <MonitoringManager.h>
#include <Logger.h>
#include <GameSession.h>
#include <ThriftException.h>
#include <cxxtools/ioerror.h>

extern MonitoringManager* monitorptr;
extern Logger* loggerptr;
using namespace std;
using namespace clooponline::packet;


GSAAAChecker::GSAAAChecker(Router* router, std::string aaaip, uint16_t aaaport, string urlpath, string gstoken) :
   AAAChecker(router), mRPCClient(aaaip, aaaport, urlpath), mGetOTPInfoCaller(mRPCClient, "getOTPInfo"),
   mGSToken(gstoken), mAuthorized(false)
{
   setProfileFactory<GSProfileFactory>();
   checkAuthorizedApps();
}

void GSAAAChecker::connectHandler(shared_ptr<const Profile> client)
{
   //TODO: identify timeout should be configurable
   mIDtoEventID.insert({client->CID, mRouter->mThreadPool.AddEvent(bind(&GSAAAChecker::identifyTimeout, this, client), 2)});
}

void GSAAAChecker::disconnectHandler(std::shared_ptr<const Profile> client)
{
   auto it = mIDtoEventID.find(client->CID);
   if (it != mIDtoEventID.end())
   {
      mRouter->mThreadPool.RemoveEvent(it->second);
      mIDtoEventID.erase(it);
   }
}

void GSAAAChecker::onPacketFromUnidentified(shared_ptr<const Profile> client, PacketParser* reqPacket, PacketParser* repPacket)
{
   if (reqPacket->getMessageType() != PacketType::IDENTIFY)
      THROW_ERROR_CODE(NOT_IDENTIFIED)

   HANDLE_METHOD_HEAD(Identify)

   GetOTPInfoResponse result = mGetOTPInfoCaller(innerpack.OTP);

   if (result.result == "ok")
   {
      if (!mAuthorized || chrono::steady_clock::now() - mLastAuthorizedAppsGet > chrono::hours(1))
      {
         checkAuthorizedApps();
      }

      if (find(mAuthorizedApps.begin(), mAuthorizedApps.end(), result.AppID) != mAuthorizedApps.end())
      {
         auto it = mIDtoEventID.find(client->CID);
         if (it != mIDtoEventID.end())
         {
            mRouter->mThreadPool.RemoveEvent(it->second);
            mIDtoEventID.erase(it);
         }
         setAuthFlag(client, true);
         auto profile = (GSProfile*)(const_cast<Profile*>(client.get()));
         profile->UID = result.UID;
         profile->AppID = result.AppID;
         ++mIdentifiedCount;

         if (loggerptr->lockBuffer(LOG_SEVERITY_INFO))
         {
            *loggerptr << "Endpoint \"" << client->CID << "\" was identified.";
            loggerptr->flushLogBuffer();
         }
      }
      else
      {
         THROW_ERROR_CODE(NOT_AUTHORIZED_APP)
      }
   }
   else
   {
      THROW_ERROR_CODE(WRONG_OTP)
   }
}

void GSAAAChecker::checkAuthorizedApps()
{
   static cxxtools::RemoteProcedure<GetAuthorizedAppsResponse, std::string>
      getAuthAppsCaller(mRPCClient, "getAuthorizedApps");
   static int fails = 0;
   GetAuthorizedAppsResponse response;

   try
   {
      response = getAuthAppsCaller(mGSToken);
   }
   catch (const cxxtools::IOError& e)
   {
      response.result = "fail";
      response.reason = e.what();
   }

   mLastAuthorizedAppsGet = std::chrono::steady_clock::now();

   if (response.result == "ok")
   {
      mAuthorizedApps = response.AuthAppIDs;
      if (!mAuthorized) mAuthorized = true;
      fails = 0;
   }
   else
   {
      if (fails >= 2)
         throw std::runtime_error(response.reason);
      else
      {
         if (loggerptr->lockBuffer(LOG_SEVERITY_WARNING))
         {
            *loggerptr << "Getting authorization from AAA was unsuccessful because: " << response.reason;
            loggerptr->flushLogBuffer();
         }
         ++fails;
      }
   }
}

void GSAAAChecker::identifyTimeout(std::shared_ptr<const Profile> client)
{
   if (client->IsConnected)
   {
      auto tp = (ThriftParser*)mRouter->createPacket();
      tp->setMessageType(PacketType::EXCEPTION);
      tp->pException.__set_errorCode(ExceptionCode::LOGIN_TIMEOUT);
      mRouter->sendPacket(client, tp);
      mRouter->closeConnection(client, "login timed out");
   }
}

void operator>>= (const cxxtools::SerializationInfo& si, GSAAAChecker::RPCResponse& re)
{
   si.getMember("result") >>= re.result;
   si.getMember("reason") >>= re.reason;
}

void operator>>= (const cxxtools::SerializationInfo& si, GSAAAChecker::GetOTPInfoResponse& re)
{
   si >>= (GSAAAChecker::RPCResponse&) re;
   si.getMember("UID") >>= re.UID;
   si.getMember("AppID") >>= re.AppID;
}

void operator>>= (const cxxtools::SerializationInfo& si, GSAAAChecker::GetAuthorizedAppsResponse& re)
{
   si >>= (GSAAAChecker::RPCResponse&) re;
   si.getMember("apps") >>= re.AuthAppIDs;
}